<?php

class Kopin_Surprise_Model_Cart_Observer extends Varien_Object
{
    public function addSurpriseToCart($observer)
    {
        /** @var Mage_Sales_Model_Quote_Item $items */
        $items  = $observer->getEvent()->getItems();
        $model = Mage::getModel('kopin_surprise/catalog_product');
        /** @var Kopin_Surprise_Model_Catalog_Product $mainProduct */
        $mainProduct = $model->load($items[0]->getData('product_id'));
        $mainProductId = $items[0]->getData('product_id');
        $helper = Mage::helper('kopin_surprise');


        if($helper->hasProductSurprise($mainProduct)&&!$helper->isProductSurprise($items[0])){
            $surpriseProductsIds=$mainProduct->getSurpriseProductIds();
            $surpriseProductsId = $surpriseProductsIds[array_rand($surpriseProductsIds)];
            /** @var Mage_Catalog_Model_Product $surpriseProduct */
            $surpriseProduct = $model->load($surpriseProductsId);
            $surpriseProduct->setCustomOption(['is_surprise'=>true])->setIsSuperMode(true);
            $minAllowedQty = Mage::getStoreConfig('kopin_surprise/settings/min_qty');
            $stockQty = $surpriseProduct->getStockItem()->getQty();
            if($stockQty>=$minAllowedQty){
                $newPrice=$items[0]->getProduct()->getPrice()-1;
                $items['0']->setPrice($newPrice);
                $items['0']->setOriginalCustomPrice($newPrice);
                $items['0']->getProduct()->setIsSuperMode(true);
                $cart = Mage::helper('checkout/cart')->getCart();
                $cart->addProduct($surpriseProduct, array(
                    'parent_id' => $mainProductId,
                    'qty' => 1,
                    'surprise' => true,
                ));
                $cart->save();
            }


        }elseif($helper->isProductSurprise($items[0])){

            $additionalOptions = array(
                'flag' => 'is_surprise',
                'colour_background' => dechex(rand(100,255)).dechex(rand(100,255)).dechex(rand(100,255)),
                'image'=>'/s/u/surprise.png',
            );
            $items[0]->addOption(array(
                'code' => 'additional_options',
                'value' => serialize($additionalOptions),
            ));
            $newPrice=1;
            $items['0']->setCustomPrice($newPrice);
            $items['0']->setOriginalCustomPrice($newPrice);
            $items['0']->getProduct()->setIsSuperMode(true);
        }
    }

    public function removeSurpriseFromCart($observer){

        $event = $observer->getEvent();

        $deleteProductId = $event->getQuoteItem()->getProductId();
        /** @var Mage_Sales_Model_Quote $quote */
        $quote=$event->getQuoteItem()->getQuote();
        /** @var Mage_Sales_Model_Entity_Quote_Item_Collection $collection */
        $collection = $quote->getItemsCollection();
        $items = $collection->getItems();
        foreach ($items as $item){
            /** @var Mage_Sales_Model_Quote_Item_Option $item */
           $options = $item->getOptions();
           $values = unserialize($options[0]->getData('value'));
           $itemId = $item->getItemId();
           if($values['parent_id']===$deleteProductId){
               Mage::helper('checkout/cart')->getCart()->removeItem($itemId)->save();
           }
        }


    }
}
<?php

class Kopin_Surprise_Model_Observer extends Varien_Object
{

    public function catalogProductPrepareSave($observer)
    {
        $event = $observer->getEvent();

        $product = $event->getProduct();
        $request = $event->getRequest();

        $links = $request->getPost('links');
        if (isset($links['surprise']) && !$product->getSurpriseReadonly()) {
            $product->setSurpriseLinkData(Mage::helper('adminhtml/js')->decodeGridSerializedInput($links['surprise']));
        }
    }

    public function catalogModelProductDuplicate($observer)
    {
        $event = $observer->getEvent();

        $currentProduct = $event->getCurrentProduct();
        $newProduct = $event->getNewProduct();

        $data = array();
        $currentProduct->getLinkInstance()->useSurpriseLinks();
        $attributes = array();

        foreach ($currentProduct->getLinkInstance()->getAttributes() as $_attribute) {
            if (isset($_attribute['code'])) {
                $attributes[] = $_attribute['code'];
            }
        }
        foreach ($currentProduct->getSurpriseLinkCollection() as $_link) {
            $data[$_link->getLinkedProductId()] = $_link->toArray($attributes);
        }
        $newProduct->setSurpriseLinkData($data);
    }

    /**
     * Add surprise product to cart if exist
     * @param Varien_Event_Observer $observer
     */

    public function placeSurpriseOrder($observer)
    {
        /** @var Kopin_Surprise_Model_Catalog_Product_Link $model */
        $model = Mage::getModel('kopin_surprise/catalog_product_link');

        $model->useSurpriseLinks()->getAttributeTypeTable('decimal');
    }
}
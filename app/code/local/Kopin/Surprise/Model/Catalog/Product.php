<?php
/**
 * Created by PhpStorm.
 * User: vitya
 * Date: 13.07.17
 * Time: 21:21
 */
class Kopin_Surprise_Model_Catalog_Product extends Mage_Catalog_Model_Product
{
    /**
     * Retrieve array of custom products
     *
     * @return array
     */
    public function getSurpriseProducts()
    {
        if (!$this->hasSurpriseProducts()) {
            $products = array();
            $collection = $this->getSurpriseProductCollection();
            foreach ($collection as $product) {
                $products[] = $product;
            }
            $this->setSurpriseProducts($products);
        }
        return $this->getData('surprise_products');
    }
    /**
     * Retrieve surprise products identifiers
     *
     * @return array
     */
    public function getSurpriseProductIds()
    {
        if (!$this->hasSurpriseProductIds()) {
            $ids = array();
            foreach ($this->getSurpriseProducts() as $product) {
                $ids[] = $product->getId();
            }
            $this->setSurpriseProductIds($ids);
        }
        return $this->getData('surprise_product_ids');
    }
    /**
     * Retrieve collection surprise product
     *
     * @return Mage_Catalog_Model_Resource_Product_Link_Product_Collection
     */
    public function getSurpriseProductCollection()
    {
        $collection = $this->getLinkInstance()->useSurpriseLinks()
            ->getProductCollection()
            ->setIsStrongMode();
        $collection->setProduct($this);
        return $collection;
    }
    /**
     * Retrieve collection surprise link
     *
     * @return Mage_Catalog_Model_Resource_Product_Link_Collection
     */
    public function getSurpriseLinkCollection()
    {
        $collection = $this->getLinkInstance()->useSurpriseLinks()
            ->getLinkCollection();
        $collection->setProduct($this);
        $collection->addLinkTypeIdFilter();
        $collection->addProductIdFilter();
        $collection->joinAttributes();
        return $collection;
    }
}
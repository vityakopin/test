<?php

/**
 * Created by PhpStorm.
 * User: vitya
 * Date: 13.07.17
 * Time: 21:12
 */
class Kopin_Surprise_Model_Catalog_Product_Link extends Mage_Catalog_Model_Product_Link
{
    const LINK_TYPE_SURPRISE   = 10;

    public function useSurpriseLinks()
    {
        $this->setLinkTypeId(self::LINK_TYPE_SURPRISE);
        return $this;
    }

    /**
     * Save data for product relations
     *
     * @param   Mage_Catalog_Model_Product $product
     * @return  Mage_Catalog_Model_Product_Link
     */
    public function saveProductRelations($product)
    {
        parent::saveProductRelations($product);

        $data = $product->getSurpriseLinkData();
        if (!is_null($data)) {
            $this->_getResource()->saveProductLinks($product, $data, self::LINK_TYPE_SURPRISE);
        }

    }

}
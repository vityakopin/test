<?php

$installer = $this;

/**
 * Install product link types
 */
$data = array(
    array(
        'link_type_id'  => Kopin_Surprise_Model_Catalog_Product_Link::LINK_TYPE_SURPRISE,
        'code'          => 'surprise'
    )
);

foreach ($data as $bind) {
    $installer->getConnection()->insertForce($installer->getTable('catalog/product_link_type'), $bind);
}

/**
 * Install product link attributes
 */
$data = array(
    array(
        'link_type_id'                  => Kopin_Surprise_Model_Catalog_Product_Link::LINK_TYPE_SURPRISE,
        'product_link_attribute_code'   => 'surprises',
        'data_type'                     => 'decimal'
    )
);

$installer->getConnection()->insertMultiple($installer->getTable('catalog/product_link_attribute'), $data);
<?php

require_once(Mage::getModuleDir('controllers','Mage_Adminhtml').DS.'Catalog'.DS.'ProductController.php');

class Kopin_Surprise_Adminhtml_Catalog_ProductController extends Mage_Adminhtml_Catalog_ProductController
{


    public function surpriseAction()
    {
        $this->_initProduct();
        $this->loadLayout();
        $this->getLayout()->getBlock('catalog.product.edit.tab.surprise')
            ->setProductsSurprise($this->getRequest()->getPost('products_surprise', null));
        $this->renderLayout();

    }

    public function surpriseGridAction()
    {
        $this->_initProduct();
        $this->loadLayout();
        $this->getLayout()->getBlock('catalog.product.edit.tab.surprise')
            ->setProductsSurprise($this->getRequest()->getPost('products_surprise', null));
        $this->renderLayout();
    }
}
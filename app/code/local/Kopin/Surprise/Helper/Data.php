<?php
class Kopin_Surprise_Helper_Data extends Mage_Core_Helper_Abstract{
    /**
     * Check is the product surprise
     * @param int|Mage_Sales_Model_Quote_Item $product
     * @return boolean
     */
    public function isItemSurprise($product){
        if (is_numeric($product)) {
            $this->loadProductById($product);
        }
        $additionalOptions = $product->getCustomOption('additional_options');
        if (isset($additionalOptions)) {
            $additionalOptions = unserialize($additionalOptions->getValue());
            if(isset($additionalOptions['flag'])&&$additionalOptions['flag']=='is_surprise'){
                return true;
            }else{
                return false;
            }
        }
    }

    /**
     * Check has the product surprise
     * @param int|Mage_Sales_Model_Quote_Item $product
     * @return boolean
     */
    public function hasProductSurprise($product)
    {
        if (is_numeric($product)) {
            $this->loadProductById($product);
        }
        if($product->getSurpriseProductIds()){
            return true;
        }else{
            return false;
        }
    }


    /**
     * Loads product by id
     * @param int $product
     * @return Kopin_Surprise_Model_Catalog_Product
     */
    public function loadProductById($product)
    {
        /** @var Kopin_Surprise_Model_Catalog_Product $product */
        $product = Mage::getModel('catalog/product')
            ->load($product);
        return $product;
    }

    /**
     * @param Mage_Sales_Model_Quote_Item $item
     * @return  boolean
     */
    public function isProductSurprise($item)
    {
        if (is_numeric($item)) {
            $this->loadProductById($item);
        }
        /** @var Mage_Sales_Model_Quote_Item $options */
        $itemOptions = $item->getOptions();
        if(!empty($itemOptions)){
            $optionsArray = unserialize($itemOptions[0]->getValue());
            if (isset ($optionsArray['surprise'])){
                return $optionsArray['surprise'];
            }
        }
        return '';
    }

    /**
     * Get question mark with background
     * @param int|Mage_Sales_Model_Quote_Item $product
     * @return array
     */
    public function changeImages($product)
    {
        if (is_numeric($item)) {
            $this->loadProductById($item);
        }
        $product->setSmallImage('/s/u/surprise.png');
        $product->setThumbnail('/s/u/surprise.png');
        return $product;
    }

    public function getBackground($item)
    {
        $options = unserialize($item->getOptions()[1]->getValue());
        if (isset($options['colour_background'])){
            return $options['colour_background'];
        }

    }
}